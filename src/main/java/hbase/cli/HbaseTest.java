package hbase.cli;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.*;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;

public final class HbaseTest {

    private static Logger logger = (Logger) MyLoggerFactory.getLogger(HbaseTest.class.getName());

    private static HBaseCli hbasecli;

    public static void main(String[] args) {

        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.ERROR);
        
        hbasecli = new HBaseCli();
        CommandLine commandLine = new CommandLine(hbasecli);

        commandLine.setExecutionStrategy(parseResult -> {
            try {
                return hbasecli.executionStrategy(parseResult);
            } catch (IOException e) {
                return -1;
            }
        });
        int exitCode = commandLine.execute(args);
        System.exit(exitCode);
    }
}
