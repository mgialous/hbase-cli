package hbase.cli;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DefaultAdminClient {
    private static Logger logger = (Logger) MyLoggerFactory.getLogger(DefaultAdminClient.class.getName());

    public static DefaultAdminClient single_instance = null;
    Connection connection = null;
    Admin admin = null;


    public synchronized static DefaultAdminClient getInstance() {
        if (single_instance == null) {
            single_instance = new DefaultAdminClient();
        }
        return single_instance;
    }

    private DefaultAdminClient() {
    }

    private String principal;
    private String keytab;
    private Configuration config;

    public Connection getConnection() {

        // Kerberos configuration
        UserGroupInformation.setConfiguration(config);

        try {
            logger.debug("Login user from keytab ... " + principal + ", " + keytab);
            UserGroupInformation.loginUserFromKeytab(principal, keytab);
            logger.debug("Creating connection...");
            connection = ConnectionFactory.createConnection(config);
            logger.debug("Connection was created.");
            return connection;
        } catch (IOException e) {
            logger.error("Failed to get connection", e);
            return null;
        }
    }

    public void initialize(String principal, String keytab, Configuration config) {
        this.principal = principal;
        this.keytab = keytab;
        this.config = config;
    }
}
