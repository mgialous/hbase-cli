package hbase.cli;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;

public class MyLoggerFactory {

    private static final Level PROJECT_LOGGER_LEVEL = Level.DEBUG;

    public static Logger getLogger(String name) {
        Logger logger = (Logger) LoggerFactory.getLogger(name);
        logger.setLevel(PROJECT_LOGGER_LEVEL);
        return logger;
    }
}
