package hbase.cli;

import ch.qos.logback.classic.Logger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.MetaTableAccessor;
import org.apache.hadoop.hbase.RegionMetrics;
import org.apache.hadoop.hbase.ServerName;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

public class OperationUtils {
    private static Logger logger = (Logger) MyLoggerFactory.getLogger(OperationUtils.class.getName());

    public static void scanTable(String tableName) {
        ResultScanner scanner = null;
        try {
            Table htable = DefaultAdminClient.getInstance().getConnection().getTable(TableName.valueOf(tableName));
            long startTime = System.currentTimeMillis();
            scanner = htable.getScanner(new Scan());
            int cnt = 0;
            logger.debug("Scanning table " + tableName + " ...");
            for (Result result = scanner.next(); result != null; result = scanner.next()) {
                cnt++;
                logger.debug("Cnt is " + cnt);
                if (cnt % 100 == 0) {
                    System.out.println("Cnt = " + cnt);
                }
            }
            long execTime = System.currentTimeMillis() - startTime;
            System.out.println("Stats for table '" + tableName + "':");
            System.out.println("Number of objects = " + cnt);
            System.out.println("Execution time of scanTable = " + execTime + "ms");
        } catch (IOException e) {
            logger.error("Failed to scan table", e);
        } finally {
            scanner.close();
        }

    }

    public static boolean tableExists(String table) {
        try {
            Connection connection = DefaultAdminClient.getInstance().getConnection();
            Admin admin = connection.getAdmin();
            TableName tableName = TableName.valueOf(table);

            // Check if table exists
            logger.debug("Checking if table exists...");
            if (admin.tableExists(tableName)) {
                System.out.println("The table " + table + " exists");
                return true;
            } else {
                System.out.println("The table " + table + " does not exist");
                return false;
            }
        } catch (IOException e) {
            logger.error("Failed to check if table exists", e);
            return false;
        }
    }

    public static void listTables() {
        try {
            Connection conn = DefaultAdminClient.getInstance().getConnection();
            Admin admin = conn.getAdmin();
            TableName [] tables = admin.listTableNames();
            for (int i = 0; i < tables.length; i++) {
                System.out.println(tables[i].getNameAsString());
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            logger.error("Failed to listTables");
            System.exit(1);
        }
    }

    public static void basicTest(String table) throws IOException {

        Connection connection = DefaultAdminClient.getInstance().getConnection();
        Admin admin = connection.getAdmin();
        long startTime = System.currentTimeMillis();

        // Table definitions
        TableName tableName = TableName.valueOf(table);
        String columnFamily = "testCF";
        String columnName = "testCN";
        String value = "testVAL";
        String row = "row1";

        if (!admin.tableExists(tableName)) {
            // Create table
            logger.debug("Creating table...");
            TableDescriptorBuilder tableDescriptorBuilder = TableDescriptorBuilder.newBuilder(tableName);
            ColumnFamilyDescriptor columnFamilyDescriptor = ColumnFamilyDescriptorBuilder.newBuilder(Bytes.toBytes(columnFamily)).build();
            tableDescriptorBuilder.setColumnFamily(columnFamilyDescriptor);
            admin.createTable(tableDescriptorBuilder.build());
        }

        // Check if table exists
        logger.debug("Checking if table exists...");
        if (!admin.tableExists(tableName)) {
            System.out.println("Table " + table + " does not exist!");
            System.exit(-1);
        }

        // Write to table
        logger.debug("Writing to table...");
        Put p = new Put(Bytes.toBytes(row));
        p.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName), Bytes.toBytes(value));
        Table htable = connection.getTable(tableName);
        htable.put(p);

        // Read Table
        logger.debug("Reading from table...");
        Get g = new Get(Bytes.toBytes(row));
        Result rs = htable.get(g);
        byte[] getVal = rs.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName));
        logger.debug("Value read = " + Bytes.toString(getVal));
        if (!value.equals(Bytes.toString(getVal))) {
            System.out.println("Value read " + Bytes.toString(getVal) + " not equal to " + value);
            System.exit(-1);
        }

        // Delete Table
        logger.debug("Deleting table...");
        admin.disableTable(tableName);
        admin.deleteTable(tableName);

        long execTime = System.currentTimeMillis() - startTime;
        System.out.println("Execution time of basicTest = " + execTime + "ms");
    }

    public static void removeEmptyRegions(String table, boolean dryRun) {

        if (dryRun) {
            System.out.println("============ DRY RUN ================");
        }

        TableName tableName = TableName.valueOf(table);
        Connection conn = DefaultAdminClient.getInstance().getConnection();
        Admin admin = null;
        Collection<ServerName> regionServers = null;
        List<RegionInfo> regionInfos = null;
        try {
            admin = DefaultAdminClient.getInstance().getConnection().getAdmin();
            regionServers = admin.getRegionServers();
            regionInfos = admin.getRegions(tableName);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        List<RegionInfo> regionsToDelete = new ArrayList<RegionInfo>();
        for (ServerName rs : regionServers) {
            List<RegionMetrics> regionMetrics = null;
            try {
                regionMetrics = admin.getRegionMetrics(rs, tableName);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
            logger.debug("Region metrics for rs " + rs.getServerName() +
                    " and table: " + table);

            for (RegionMetrics rm : regionMetrics) {
                logger.debug("RegionMetrics " + rm.getNameAsString() +
                        "\nstoreFileCount: " + rm.getStoreFileCount() +
                        "\nstoreFileSize: " + rm.getStoreFileSize().getLongValue() +
                        "\nmemStoreSize: " + rm.getMemStoreSize().getLongValue());

                // If both memstore and storefilesize equals 0 (CORNER CASE if <0 MB but has data)`
                if ((rm.getStoreFileSize().getLongValue() == 0) && (rm.getMemStoreSize().getLongValue() == 0)) {
                    for (RegionInfo regionInfo : regionInfos) {
                        if (regionInfo.getRegionNameAsString().equals(rm.getNameAsString())) {
                            regionsToDelete.add(regionInfo);
                            logger.debug("RegionInfo to be deleted = " + regionInfo.getRegionNameAsString());
                            break;
                        }
                    }
                }
            }
        }

        try {
            // Execute if not dry run
            if (!dryRun) {
                FileSystem hdfs = getHdfs();
                for (RegionInfo regionToDelete : regionsToDelete) {

                    // Close region (unassign)
                    admin.unassign(regionToDelete.getRegionName());
                    logger.debug("UNASSIGNED region: " + regionToDelete.getRegionNameAsString());

                    // Remove from meta (explicitly)
                    MetaTableAccessor.deleteRegionInfo(conn, regionToDelete);
                    logger.debug("DELETED region: " + regionToDelete.getRegionNameAsString());

                    // Delete hdfs
                    String[] tmp = regionToDelete.getRegionNameAsString().split(":|,|\\.");
                    Path path = new Path("hdfs://"+ HBaseCli.getCluster().getNetworkAddress() + "/hbase/data/" + tmp[0] +
                            "/" + tmp[1] +
                            "/" + tmp[tmp.length - 1]);
                    hdfs.delete(path, true);
                    logger.debug("Deleted path: " + path.toString());
                }
            } else {
                System.out.println("Regions to delete:");
                for (RegionInfo regionToDelete : regionsToDelete) {
                    System.out.println(regionToDelete.getRegionNameAsString());
                }

            }

            // In any case write the output to the file
            StringBuilder sb = new StringBuilder("");
            for (RegionInfo regionToDelete : regionsToDelete) {
                sb.append(regionToDelete.getRegionNameAsString());
            }
            String fileExtension = new SimpleDateFormat("'.txt.'MMdd-HHmmss").format(new Date());
            Files.write(Paths.get("/tmp/deleteRegions" + fileExtension), sb.toString().getBytes());
            System.out.println("The list of the empty regions can be found under the /tmp/deleteRegions"+ fileExtension +" file");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static FileSystem getHdfs() {
        Configuration configuration = new Configuration();
        configuration.addResource(new Path(HBaseCli.getCoreSitePath()));
        configuration.addResource(new Path(HBaseCli.getHdfsSitePath()));
        configuration.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        configuration.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        configuration.set("hadoop.security.authentication", "Kerberos");
        UserGroupInformation.setConfiguration(configuration);
        try {
            return FileSystem.get(new URI("hdfs://" + HBaseCli.getCluster().getNetworkAddress() + ":8020/"), configuration);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            logger.error("Could not get hdfs FileSystem");
            System.exit(1);
        }
        // Should not reach here
        return null;
    }

    public static void flushTables(boolean flushAll, boolean dryRun) {
        if (dryRun) {
            System.out.println("============ DRY RUN ================");
        }




    }

    public static void filter(String table) throws IOException {
        Connection connection = DefaultAdminClient.getInstance().getConnection();
        Admin admin = connection.getAdmin();


        // Table definitions
        TableName tableName = TableName.valueOf(table);
        String columnFamily = "testCF";
        String columnName = "testCN";
        String value = "testVAL";
        String row = "row1";

        if (!admin.tableExists(tableName)) {
            // Create table
            logger.debug("Creating table...");
            TableDescriptorBuilder tableDescriptorBuilder = TableDescriptorBuilder.newBuilder(tableName);
            ColumnFamilyDescriptor columnFamilyDescriptor = ColumnFamilyDescriptorBuilder.newBuilder(Bytes.toBytes(columnFamily)).build();
            tableDescriptorBuilder.setColumnFamily(columnFamilyDescriptor);
            admin.createTable(tableDescriptorBuilder.build());
        }

        // Check if table exists
        logger.debug("Checking if table exists...");
        if (!admin.tableExists(tableName)) {
            System.out.println("Table " + table + " does not exist!");
            System.exit(-1);
        }

        // Write to table
        logger.debug("Writing to table...");
        Put p = new Put(Bytes.toBytes(row));
        //p.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName), Bytes.toBytes(value));
        Table htable = connection.getTable(tableName);
        //htable.put(p);




        // vv TimestampFilterExample
        List<Long> ts = new ArrayList<Long>();
        ts.add(new Long(1633354408));
        ts.add(new Long(1633354409)); // co TimestampFilterExample-1-AddTS Add timestamps to the list.
        ts.add(new Long(1633354410));
        Filter filter = new TimestampsFilter(ts);


        Scan scan1 = new Scan();
        scan1.setFilter(filter); // co TimestampFilterExample-2-AddFilter Add the filter to an otherwise default Scan instance.
        ResultScanner scanner1 = htable.getScanner(scan1);
        // ^^ TimestampFilterExample
        System.out.println("Results of scan #1:");
        // vv TimestampFilterExample
        for (Result result : scanner1) {
            System.out.println(result);
        }
        scanner1.close();

        Scan scan2 = new Scan();
        Filter rowFilter = new RowFilter(CompareFilter.CompareOp.EQUAL, // co RowFilterExample-3-Filter3 The third filter uses a substring match approach.
                new SubstringComparator("row2"));
        scan2.setFilter(rowFilter);
        //1633354409000L
        scan2.setTimeRange(new Long(1633354407000L),new Long(Long.MAX_VALUE)); // co TimestampFilterExample-3-AddTSRange Also add a time range to verify how it affects the filter
        ResultScanner scanner2 = htable.getScanner(scan2);
        // ^^ TimestampFilterExample
        System.out.println("Results of scan #2:");
        // vv TimestampFilterExample
        for (Result result : scanner2) {
            System.out.println(result);
        }
        scanner2.close();

        // Delete Table
        logger.debug("Deleting table...");
        //admin.disableTable(tableName);
        //admin.deleteTable(tableName);
    }

    private boolean isTableEmpty(String table) throws IOException {
        TableName tableName = TableName.valueOf(table);
        Connection conn = DefaultAdminClient.getInstance().getConnection();
        Admin admin = conn.getAdmin();

        // Check if table exists
        logger.debug("Checking if table exists...");
        if (!admin.tableExists(tableName)) {
            System.out.println("Table " + table + " does not exist!");
            System.exit(-1);
        }

        Table htable = conn.getTable(tableName);
        ResultScanner rsScanner = htable.getScanner(new Scan());
        Result[] result = rsScanner.next(1);
        if (result.length != 0) {
            logger.debug("There is at least one row in the table, so it is not empty");
            return false;
        } else {
            return true;
        }
    }
}
