package hbase.cli;

import ch.qos.logback.classic.Logger;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.ParseResult;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Properties;

@CommandLine.Command(name = "HbaseCli", versionProvider = HBaseCli.PropertiesVersionProvider.class,
        subcommands = {ActionSubcommand.class},
        mixinStandardHelpOptions = true)
public class HBaseCli implements Runnable {
    private static Logger logger = (Logger) MyLoggerFactory.getLogger(HBaseCli.class.getName());

    // Command line arguments
    @CommandLine.Spec
    CommandLine.Model.CommandSpec spec;

    @CommandLine.Option(required = true, names = {"-cl", "--cluster"}, description = "Valid clusters: ${COMPLETION-CANDIDATES}")
    private static HBaseCliConstants.Clusters cluster = null;

    public static HBaseCliConstants.Clusters getCluster() {
        return cluster;
    }

    @CommandLine.Option(names = {"-k", "--keytab"}, description = "Kerberos keytab")
    private static String keytab = "";

    @CommandLine.Option(names = {"-cs", "--core-site"}, description = "Path to core-site.xml")
    private static String coreSitePath = "core-site.xml";

    public static String getCoreSitePath() {
        return coreSitePath;
    }

    @CommandLine.Option(names = {"-hbs", "--hbase-site"}, description = "Path to hbase-site.xml")
    private static String hbaseSitePath = "hbase-site.xml";

    public static String getHbaseSitePath() {
        return hbaseSitePath;
    }

    @CommandLine.Option(names = {"-hds", "--hdfs-site"}, description = "Path to hdfs-site.xml")
    private static String hdfsSitePath = "hdfs-site.xml";

    public static String getHdfsSitePath() {
        return hdfsSitePath;
    }

    // ---------------------------------------- Command line arguments end ---------------------------------------- //


    public int executionStrategy(ParseResult parseResult) throws IOException {
        init(); // custom initialization to be done before executing any command or subcommand
        return new CommandLine.RunLast().execute(parseResult); // default execution strategy
    }

    @Override
    public void run() {
        // if the command was invoked without subcommand, show the usage help
        spec.commandLine().usage(System.err);
    }

    // Initialize state for parent command here
    private void init() throws IOException {

        try (
                InputStream in = getClass().getClassLoader().getResourceAsStream("krb5.conf");
                OutputStream out = new FileOutputStream(new File("krb5.conf"));) {
            IOUtils.copy(in, out);
        }

        // If default hbase.keytab is used, unpack from the resource folder
        if (keytab.equals("")) {
            keytab = "hbase.keytab";
            try (
                    InputStream in = getClass().getClassLoader().getResourceAsStream(keytab);
                    OutputStream out = new FileOutputStream(new File(keytab));) {
                IOUtils.copy(in, out);
            }
        }

        System.setProperty("java.security.krb5.conf", "krb5.conf");
        System.setProperty("hadoop.home.dir", "/");
        System.setProperty("sun.security.krb5.debug", "false");

        // Read hbase host configuration
        logger.debug("Creating HbaseConfiguration...");
        Configuration config = HBaseConfiguration.create();
        config.addResource(new Path(coreSitePath));
        config.addResource(new Path(hbaseSitePath));
        config.set("hadoop.security.authentication", "Kerberos");

        // Initialize DefaultAdmin with the appropriate values taken from cmdline
        DefaultAdminClient.getInstance().initialize(cluster.getPrincipal(), keytab, config);
    }

    static class PropertiesVersionProvider implements CommandLine.IVersionProvider {
        public String[] getVersion() throws Exception {
            final Properties properties = new Properties();
            properties.load(HbaseTest.class.getClassLoader().getResourceAsStream("project.properties"));
            return new String[]{properties.getProperty("version")};
        }
    }

}
