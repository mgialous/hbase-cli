%{!?dist: %define dist .el7.cern}
%define debug_package %{nil}
%define __maven_requires /bin/true
%undefine scl

Name: cerndb-sw-hbase-cli		
Version: 1.0
Release: 3%{?dist}
Summary: CERN HBase CLI

Group: System
BuildArch: noarch
License: CERN
URL: https://gitlab.cern.ch/mgialous/hbase-cli
Source0: %{name}-%{version}.tar.gz	
BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildRequires: java-1.8.0-openjdk
BuildRequires: maven-local
Requires: java-headless >= 1:1.8

%description
CERN HBase CLI

%prep
%setup -q
%mvn_file : %{name}/%{name} %{name}

%build
%mvn_build -f -j -- -Dmaven.repo.local=".m2"

%install
%mvn_install
install target/%{name}-%{version}*-jar-with-dependencies.jar $RPM_BUILD_ROOT/%{_javadir}/%{name}/%{name}-%{version}-jar-with-dependencies.jar

%post
ln -sf %{_javadir}/%{name}/%{name}-%{version}-jar-with-dependencies.jar  %{_javadir}/%{name}-jar-with-dependencies.jar


%clean
rm -rf $RPM_BUILD_ROOT

%files -f .mfiles
%attr(0644,root,root) %{_javadir}/%{name}/%{name}-%{version}-jar-with-dependencies.jar
%{_javadir}/%{name}/%{name}-%{version}-jar-with-dependencies.jar

%changelog
* Tue Feb 16 2021 Miltiadis Gialousis <miltiadis.gialousisr@cern.ch> 1.0.3
- Add interceptor for parsing palo alto logs

