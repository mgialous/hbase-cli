package hbase.cli;

import ch.qos.logback.classic.Logger;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.io.IOException;

enum Actions {
    scan, checkExists, removeEmptyRegions, testAction, basicTest, listTables, flushTables, filter;
}

@Command(name = "action", description = "Define the action to perform")
public class ActionSubcommand implements Runnable {
    private static Logger logger = (Logger) MyLoggerFactory.getLogger(ActionSubcommand.class.getName());

    @Parameters(arity = "1", paramLabel = "<action>", description = "Valid actions: ${COMPLETION-CANDIDATES}")
    Actions action = null;

    @Option(names = {"-t", "--tableName"}, required = false,
            description = "Name of the table in the form <namespace>:<tablename>")
    String tableName = null;

    @Option(names = {"--whitelist-"}, required = false,
            description = "Path to the file containing the whitelisted tables that should not be flushed")
    String  whiteList = null;

    @Option(names = {"--dry-run"}, required = false,
            description = "Do not proceed with the deletion of the empty regions")
    boolean dryRun = false;

    @CommandLine.Spec
    CommandLine.Model.CommandSpec spec;


    @Override
    public void run() {
        System.out.println("Action = " + action);

        switch (action) {
            case scan:
                validateMissingParameter(action, tableName);
                OperationUtils.scanTable(tableName);
                break;
            case checkExists:
                validateMissingParameter(action, tableName);
                OperationUtils.tableExists(tableName);
                break;
            case listTables:
                OperationUtils.listTables();
                break;
            case basicTest:
                validateMissingParameter(action, tableName);
                try {
                    OperationUtils.basicTest(tableName);
                } catch (IOException e) {
                    logger.error("Failed to execute basicTest", e);
                }
                break;
            case removeEmptyRegions:
                validateMissingParameter(action, tableName);
                if (OperationUtils.tableExists(tableName)) {
                    OperationUtils.removeEmptyRegions(tableName, dryRun);
                }
                break;
            case flushTables:
                validateMissingParameter(action, whiteList);
                //OperationUtils.flushTables(whiteList, dryRun);
                break;
            case filter:
                try {
                    OperationUtils.filter(tableName);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            case testAction:
                break;
        }
    }

    private void validateMissingParameter(Actions action, Object parameter) {
        if (parameter == null) {
            System.out.println("Option tableName is required for action " + action.name());
            spec.commandLine().usage(System.err);
            System.exit(1);
        }
    }
}
