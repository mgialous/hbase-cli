package hbase.cli;

public class HBaseCliConstants {

    public enum Clusters {

        MMARTINM("ithdpdev-mmartinm01.cern.ch", "188.184.80.196", "hbase/ithdpdev-mmartinm01.cern.ch@CERN.CH"),
        MGIALOUS("ithdpdev-mgialous01.cern.ch", "188.185.87.164", "hbase/ithdpdev-mgialous01.cern.ch@CERN.CH"),
        EKLESZCZ("ithdpdev-ekleszcz01.cern.ch", "137.138.44.209", "hbase/ithdpdev-ekleszcz01.cern.ch@CERN.CH"),
        ANALYTIX("ithdp1101.cern.ch", "10.21.2.68", "hbase/ithdp1101.cern.ch.cern.ch@CERN.CH"),
        LXHADOOP("ithdp1201.cern.ch", "10.21.2.73", "hbase/ithdp1201.cern.ch.cern.ch@CERN.CH"),
        NXCALS_DEV("ithdp-nxcals-dev-1.cern.ch", "137.138.62.199", "hbase/ithdp-nxcals-dev-1.cern.ch@CERN.CH"),
        NXCALS_QA("ithdp-nxcals-qa-1.cern.ch", "137.138.76.165", "hbase/ithdp-nxcals-qa-1.cern.ch@CERN.CH"),
        NXCALS_PROD("ithdp1001.cern.ch", "188.184.1.76", "hbase/ithdp1001.cern.ch@CERN.CH");

        private final String hostname;
        private final String ip;
        private final String principal;
        Clusters(String hostname, String ip, String principal) {
            this.hostname = hostname;
            this.ip = ip;
            this.principal = principal;
        }

        public String getHostname() {
            return hostname;
        }

        public String getIp() {
            return ip;
        }

        public String getPrincipal() {
            return principal;
        }

        public String getNetworkAddress() {
            if (!ip.isEmpty()) {
                return ip;
            }
            if (!hostname.isEmpty()) {
                return hostname;
            }

            return null;
        }
    }
}
