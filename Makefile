SPECFILE            = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME		= $(shell awk '$$1 == "Name:" 	  { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION    = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
SPECFILE_RELEASE    = $(shell awk '$$1 == "Release:"  { print $$2 }' $(SPECFILE) )
DIST               ?= $(shell rpm --eval %{dist})
TARFILE             = $(SPECFILE_NAME)-$(SPECFILE_VERSION).tar.gz

sources:
		@echo $(TARFILE)
		@echo $(SPECFILE_NAME)
		@echo $(SPECFILE_VERSION)
		@echo $(SPECFILE_RELEASE)
		@echo $(DIST)
		mvn -Dmaven.repo.local="$(PWD)/.m2" surefire:help dependency:go-offline
		tar -zcvf $(TARFILE) --exclude-vcs --transform 's,^,$(SPECFILE_NAME)-$(SPECFILE_VERSION)/,' src/* .m2/* pom.xml

clean:
		git clean -Xdf
		rm -rf target/
		rm -rf SPECS
		rm -rf SOURCES
		rm -rf $(PKGNAME)
		rm -rf SRPMS
		rm -rf RPMS
		rm -rf BUILD
		rm -rf BUILDROOT

rpm: sources
		rpmbuild -bb $(SPECFILE) --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define "_sourcedir $(PWD)" $(SPECFILE)

srpm: sources
		rpmbuild -bs $(SPECFILE) --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define "_sourcedir $(PWD)" $(SPECFILE)

all:	sources

